#Autor:Christian Favian Condoy Carrilo
#Email:christian.cndoy@unl.edu.ec
"""Programa que imprime si el numero ingresado es primo o no primo con un bucle repetitivo mientra no se ingrese fin
 el programa no terminara y seguira pidiendo numero"""
def numero_primo():
    while True:
        try:
            numero = input("|Ingrese un Numero o (fin para terminar|:")
            contador = 0
            if numero.lower() in "fin":
                print()
                print("Programa Terminado")
                break
            numero = int(numero)
            for i in range(1, numero+1):
                if numero % i == 0:
                    contador +=1
            if contador == 2:
                    print("El numero", [numero], "es Primo.")
            else:
                    print("El numero ", [numero], " No es Primo.")

        except ValueError:
                    print("Ingrese un numero Valido!")
numero_primo()
